package com.mli;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.mli.ClientIdGeneratorApplication;


@SpringBootTest(classes = ClientIdGeneratorApplication.class)
public class CustomeridApplicationTest {

	@Test
	public void contextLoads() {
		ClientIdGeneratorApplication.main(new String[]{});
	}
}
