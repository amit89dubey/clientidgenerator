package com.mli.auth.controller;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import com.mli.auth.request.CognitoApiRequest;
import com.mli.auth.request.CognitoRequest;
import com.mli.auth.request.CognitoRequestPayload;
import com.mli.auth.response.CognitoApiResponse;
import com.mli.auth.response.Header;
import com.mli.common.utils.MessageConstants;
/**
 * @author Amit
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class AuthTokenGeneratorRestControllerTests {
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private Environment env;

	static String protocolSubstringWithDomain;
	static String authTokenGeneratePath;
	static CognitoApiRequest tokenReq;
	static CognitoRequest request;
	static Header header;
	static CognitoRequestPayload payload;
	static CognitoApiRequest badTokenReq;
	
	static String responseStr;
	static String payloadStr;
	static String msgInfoStr;
	static String msgCodeStr;
	static String accessTokenStr;
	static String expiresInStr;
	static String tokenTypeStr;
	static String errorMessageStr;

	@BeforeAll
	public static void init(){
		protocolSubstringWithDomain = "com.mli.protocolSubstringWithDomain";
		authTokenGeneratePath = "com.mli.auth.tokenGeneratePath";
		tokenReq = new CognitoApiRequest();
		request = new CognitoRequest();
		header = new Header();
		payload = new CognitoRequestPayload();
		header.setAppId("UNIT_TESTING");
		header.setCorrelationId(UUID.randomUUID().toString());
		request.setHeader(header);
		request.setPayload(payload);
		tokenReq.setRequest(request);
		badTokenReq = new CognitoApiRequest();
		
		responseStr="response";
		payloadStr="payload";
		msgInfoStr="msgInfo";
		msgCodeStr="msgCode";
		accessTokenStr="accessToken";
		expiresInStr="expiresIn";
		tokenTypeStr="tokenType";
		errorMessageStr="errorMessage";
	}

	@AfterAll
	public static void destroy() {
		protocolSubstringWithDomain = null;
		authTokenGeneratePath = null;
		tokenReq = null;
		request = null;
		header = null;
		payload = null;
		badTokenReq = null;
		responseStr=null;
		payloadStr=null;
		msgInfoStr=null;
		msgCodeStr=null;
		accessTokenStr=null;
		expiresInStr=null;
		tokenTypeStr=null;
		errorMessageStr=null;
	}

	@Test
	void processAuthTokenGenerationTest() throws RestClientException, MalformedURLException {
		payload.setClientId(env.getProperty("com.mli.auth.clientid"));
		payload.setClientSecret(env.getProperty("com.mli.auth.clientSecret"));
		tokenReq.getRequest().setPayload(payload);
		ResponseEntity<Map> response = restTemplate.postForEntity(
				new URL(env.getProperty(protocolSubstringWithDomain) + port + env.getProperty(authTokenGeneratePath)).toString(), tokenReq,
				Map.class);
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNotNull(response.getBody());
		assertNotNull(((Map)(((Map)(response.getBody().get(responseStr))).get(msgInfoStr))).get(msgCodeStr));
		assertEquals(MessageConstants.C200,(((Map)(((Map)(response.getBody().get(responseStr))).get(msgInfoStr))).get(msgCodeStr)));
		assertNotNull(((Map)(((Map)(response.getBody().get(responseStr))).get(payloadStr))).get(accessTokenStr));
		assertNotNull(((Map)(((Map)(response.getBody().get(responseStr))).get(payloadStr))).get(expiresInStr));
		assertNotNull(((Map)(((Map)(response.getBody().get(responseStr))).get(payloadStr))).get(tokenTypeStr));
	}
	
	@Test
	void processAuthTokenGenerationTestNegaive1() throws RestClientException, MalformedURLException {
		ResponseEntity<CognitoApiResponse> response = restTemplate.postForEntity(
				new URL(env.getProperty(protocolSubstringWithDomain) + port + env.getProperty(authTokenGeneratePath)).toString(), null,
				CognitoApiResponse.class);
		assertNotNull(response);
		assertEquals(HttpStatus.UNSUPPORTED_MEDIA_TYPE, response.getStatusCode());
		assertNotNull(response.getBody());
		assertEquals(null, response.getBody().getResponse());
	}

	@Test
	void processAuthTokenGenerationTestNegaive2() throws RestClientException, MalformedURLException {
		ResponseEntity<CognitoApiResponse> response = restTemplate.postForEntity(
				new URL(env.getProperty(protocolSubstringWithDomain) + port + env.getProperty(authTokenGeneratePath)).toString(), badTokenReq,
				CognitoApiResponse.class);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertNotNull(response.getBody());
		assertNotNull(response.getBody().getResponse().getMsgInfo().getMsgCode());
		assertEquals(MessageConstants.C600,response.getBody().getResponse().getMsgInfo().getMsgCode());
	}
	
	@Test
	void processAuthTokenGenerationTestNegative3() throws RestClientException, MalformedURLException {
		payload.setClientId("12345");
		tokenReq.getRequest().setPayload(payload);
		ResponseEntity<Map> response = restTemplate.postForEntity(
				new URL(env.getProperty(protocolSubstringWithDomain) + port + env.getProperty(authTokenGeneratePath)).toString(), tokenReq,
				Map.class);
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNotNull(response.getBody());
		assertNotNull(((Map)(((Map)(response.getBody().get(responseStr))).get(msgInfoStr))).get(msgCodeStr));
		assertEquals(MessageConstants.C500,(((Map)(((Map)(response.getBody().get(responseStr))).get(msgInfoStr))).get(msgCodeStr)));
		assertNotNull(((Map)(((Map)(response.getBody().get(responseStr))).get(payloadStr))).get(errorMessageStr));
	}
}
