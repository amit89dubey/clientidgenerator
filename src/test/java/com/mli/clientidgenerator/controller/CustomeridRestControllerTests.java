package com.mli.clientidgenerator.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import com.mli.clientidgenerator.request.APIRequest;
import com.mli.clientidgenerator.request.RequestData;
import com.mli.clientidgenerator.request.RequestPayLoad;
import com.mli.clientidgenerator.response.APIResponse;
import com.mli.clientidgenerator.response.HeaderEntity;
import com.mli.common.utils.MessageConstants;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class CustomeridRestControllerTests {
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private Environment env;
	
	static String protocolSubstringWithDomain;
	static String customerIdGeneratePath;
	static APIRequest customerIdReq;
	static RequestData request;
	static HeaderEntity header;
	static RequestPayLoad payload;
	static APIRequest badCustomerIdReq;

	@BeforeAll
	public static void init(){
		protocolSubstringWithDomain = "com.mli.protocolSubstringWithDomain";
		customerIdGeneratePath = "com.mli.auth.clientIdGeneratePath";
		customerIdReq = new APIRequest();
		request = new RequestData();
		header = new HeaderEntity();
		payload = new RequestPayLoad();
		header.setSoaAppId("UNIT_TESTING");
		header.setSoaCorrelationId(UUID.randomUUID().toString());
		payload.setAppTransactionId("UNIT_TESTING TRANSACTION");
		request.setHeader(header);
		request.setPayload(payload);
		customerIdReq.setRequest(request);
		badCustomerIdReq = new APIRequest();
	}

	@AfterAll
	public static void destroy() {
		protocolSubstringWithDomain = null;
		customerIdGeneratePath = null;
		customerIdReq = null;
		request = null;
		header = null;
		payload = null;
		badCustomerIdReq = null;
	}

	@Test
	void processCustomerIDGenerationTest() throws RestClientException, MalformedURLException {
		ResponseEntity<APIResponse> response = restTemplate.postForEntity(
				new URL(env.getProperty(protocolSubstringWithDomain) + port + env.getProperty(customerIdGeneratePath)).toString(), customerIdReq,
				APIResponse.class);
		assertNotNull(response);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNotNull(response.getBody());
		assertNotNull(response.getBody().getResponse().getMsgInfo().getMsgCode());
		assertEquals(MessageConstants.C200,response.getBody().getResponse().getMsgInfo().getMsgCode());
		assertNotNull(response.getBody().getResponse().getPayload().getClientId());
	}

	@Test
	void processCustomerIDGenerationTestNegaive1() throws RestClientException, MalformedURLException {
		ResponseEntity<APIResponse> response = restTemplate.postForEntity(
				new URL(env.getProperty(protocolSubstringWithDomain) + port + env.getProperty(customerIdGeneratePath)).toString(), null,
				APIResponse.class);
		assertNotNull(response);
		assertEquals(HttpStatus.UNSUPPORTED_MEDIA_TYPE, response.getStatusCode());
		assertNotNull(response.getBody());
		assertEquals(null, response.getBody().getResponse());
	}

	@Test
	void processCustomerIDGenerationTestNegaive2() throws RestClientException, MalformedURLException {
		ResponseEntity<APIResponse> response = restTemplate.postForEntity(
				new URL(env.getProperty(protocolSubstringWithDomain) + port + env.getProperty(customerIdGeneratePath)).toString(), badCustomerIdReq,
				APIResponse.class);
		assertNotNull(response);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertNotNull(response.getBody());
		assertNotNull(response.getBody().getResponse().getMsgInfo().getMsgCode());
		assertEquals(MessageConstants.C600,response.getBody().getResponse().getMsgInfo().getMsgCode());
	}
}
