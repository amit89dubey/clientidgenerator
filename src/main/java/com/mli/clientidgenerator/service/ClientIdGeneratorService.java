package com.mli.clientidgenerator.service;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.mli.clientidgenerator.request.APIRequest;
import com.mli.clientidgenerator.response.APIResponse;

public interface ClientIdGeneratorService {
	public ResponseEntity<APIResponse> generateClientID(APIRequest customerIdReq,BindingResult result);
}
