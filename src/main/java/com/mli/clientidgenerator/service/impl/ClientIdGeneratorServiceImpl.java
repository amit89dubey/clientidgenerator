package com.mli.clientidgenerator.service.impl;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.mli.clientidgenerator.dao.ClientIdGeneratorDao;
import com.mli.clientidgenerator.request.APIRequest;
import com.mli.clientidgenerator.response.APIResponse;
import com.mli.clientidgenerator.response.HeaderEntity;
import com.mli.clientidgenerator.response.ResponseDataEntity;
import com.mli.clientidgenerator.response.ResponsePayLoad;
import com.mli.clientidgenerator.service.ClientIdGeneratorService;
import com.mli.common.response.StatusEntity;
import com.mli.common.utils.MessageConstants;
import com.mongodb.MongoWriteException;

/**
 * @author Amit
 *
 */
@Service
public class ClientIdGeneratorServiceImpl implements ClientIdGeneratorService  {

	private static Logger logger = LogManager.getLogger(ClientIdGeneratorServiceImpl.class);
	@Autowired ClientIdGeneratorDao clientIdGeneratorDao;
	@Autowired Environment env;
	@Override
	public ResponseEntity<APIResponse> generateClientID(APIRequest clientIdReq,BindingResult bindingResult) 
	{
		APIResponse apiResponse = new APIResponse();
		ResponsePayLoad response = new ResponsePayLoad();
		StatusEntity msgInfo = new StatusEntity();
		ResponseDataEntity responsePayload = new ResponseDataEntity();
		try
		{
			if (bindingResult.hasErrors() || clientIdRequestValidator(clientIdReq))
			{
				msgInfo.setMsg(MessageConstants.FAILURE);
				msgInfo.setMsgCode(MessageConstants.C600);
				msgInfo.setMsgDescription(MessageConstants.C600DESC);
				response.setMsgInfo(msgInfo);
				apiResponse.setResponse(response);
				logger.debug(MessageConstants.C600DESC);
				return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
			}
			else
			{
				ThreadContext.push(clientIdReq.getRequest().getHeader().getSoaCorrelationId());
				logger.debug("Validation Success Processing further");
				response.setHeader(clientIdReq.getRequest().getHeader());
				responsePayload.setAppTransactionId(clientIdReq.getRequest().getPayload().getAppTransactionId());
				processClientIDGeneration(msgInfo,responsePayload,clientIdReq);
			} 
		} 
		catch (Exception e)
		{
			msgInfo.setMsg(MessageConstants.FAILURE);
			msgInfo.setMsgCode(MessageConstants.C500);
			msgInfo.setMsgDescription(MessageConstants.C500DESC);
			logger.error("We face Exception while generating unique client ID : "+e);
		}
		finally
		{
			response.setMsgInfo(msgInfo);
			response.setPayload(responsePayload);
			apiResponse.setResponse(response);
			saveAuditLog(apiResponse, clientIdReq);
			logger.info("processClientIDGeneration : Ended");
			ThreadContext.pop();
		}
		return new ResponseEntity<>(apiResponse, HttpStatus.OK);
	}
	
	private boolean clientIdRequestValidator(APIRequest clientIdReq)
	{
		List<String> appidList = Arrays.asList(env.getProperty(MessageConstants.APPID_COLLECTION, String[].class));
		return clientIdReq==null 
				|| clientIdReq.getRequest()==null 
				|| clientIdReq.getRequest().getHeader()==null
				|| clientIdReq.getRequest().getHeader().getSoaAppId()==null
				|| clientIdReq.getRequest().getHeader().getSoaAppId().isEmpty()
				|| clientIdReq.getRequest().getHeader().getSoaCorrelationId()==null
				|| clientIdReq.getRequest().getHeader().getSoaCorrelationId().isEmpty()
				|| clientIdReq.getRequest().getPayload()==null
				|| clientIdReq.getRequest().getPayload().getAppTransactionId()==null
				|| clientIdReq.getRequest().getPayload().getAppTransactionId().isEmpty()
				|| appidList==null
				|| !appidList.contains(clientIdReq.getRequest().getHeader().getSoaAppId());
	}

	private void saveAuditLog(APIResponse apiResponse, APIRequest clientIdReq)
	{
		new Thread(() -> {
			try 
			{
				String auditLogId = clientIdGeneratorDao.saveClientIdGeneratorAuditLog(apiResponse, clientIdReq,  apiResponse.getResponse().getPayload().getClientId());
				logger.debug("Audit Log saved with ID : Failure : " + auditLogId);
			}
			catch (Exception e) 
			{
				logger.error("We face Exception while Saving Audit Log in DB : "+e);
			}
			finally
			{
				ThreadContext.pop();
			}
		}).start();
	}

	private void processClientIDGeneration(StatusEntity msgInfo, ResponseDataEntity responsePayload,APIRequest clientIdReq)
	{

		String clientId = clientIdGeneratorDao.generateClientID();
		logger.debug("Generated Client ID is : "+clientId);
		clientId = verifyClientIDForUniqueAndReTry(clientIdReq.getRequest().getHeader(), clientId,clientIdReq.getRequest().getPayload().getAppTransactionId(), 0);
		logger.debug("Verified Client ID is : "+clientId);

		if (clientId!=null && !clientId.isEmpty()) 
		{
			msgInfo.setMsg(MessageConstants.SUCCESS);
			msgInfo.setMsgCode(MessageConstants.C200);
			msgInfo.setMsgDescription(MessageConstants.C200DESC);
			logger.debug(MessageConstants.C200DESC);
		}
		else 
		{
			msgInfo.setMsg(MessageConstants.FAILURE);
			msgInfo.setMsgCode(MessageConstants.C700);
			msgInfo.setMsgDescription(MessageConstants.C700DESC);
			logger.debug(MessageConstants.C700DESC);
		}
		responsePayload.setClientId(clientId);
	}

	private String verifyClientIDForUniqueAndReTry(HeaderEntity headerData , String clientId, String appTransactionId, int retry)
	{
		String insertResult=null;
		try 
		{
			insertResult = clientIdGeneratorDao.saveClientIDDetails(headerData, clientId, appTransactionId);
		}
		catch (MongoWriteException e) 
		{
			insertResult=null;
			logger.error("Duplicate Client ID Generated while captured while inserting MongoWriteException : "+e);
		}
		catch(Exception e)
		{
			insertResult=null;
			logger.error("Duplicate Client ID Generated while captured while inserting Exception : "+e);
		}
		if(insertResult!=null && insertResult.equalsIgnoreCase(clientId))
		{
			logger.debug("Client id generation and insertion is success and unique so do nothing");
		}
		else
		{
			logger.debug("Client id generation and insertion is not same so going to start Retry mechanism : "+retry);
			int noOfRetry = Integer.parseInt(env.getProperty("com.mli.noOfRetry"));
			logger.debug("No of retry configured is : "+noOfRetry);
			try 
			{
				int retryWaitTimeInMilliSecond = Integer.parseInt(env.getProperty("com.mli.retryWaitTimeInMilliSecond"));
				logger.debug("retryWaitTimeInMilliSecond is : "+retryWaitTimeInMilliSecond + " so sleeping thread initiated");
				Thread.sleep(retryWaitTimeInMilliSecond);
			} 
			catch (NumberFormatException | InterruptedException e) 
			{
				logger.error("We face Exception while sleeping the thred in retry mechanism : "+e);
			}
			if(retry<noOfRetry)
			{
				clientId = clientIdGeneratorDao.generateClientID();
				logger.debug("Generated Client ID is : in retry : "+clientId);
				clientId = verifyClientIDForUniqueAndReTry(headerData, clientId,appTransactionId, ++retry);
				logger.debug("Verified Client ID is : in retry : "+clientId);
			}
			else
			{
				clientId=null;
				logger.debug("Client ID generation in retry fail and set as : "+clientId);
			}
		}
		return clientId;
	}
}
