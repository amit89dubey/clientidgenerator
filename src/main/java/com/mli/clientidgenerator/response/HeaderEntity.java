package com.mli.clientidgenerator.response;

import java.io.Serializable;
/**
 * @author Amit
 *
 */
public class HeaderEntity implements Serializable{
	private static final long serialVersionUID = 7294416692517101859L;

	private String soaCorrelationId;

	private String soaAppId;

	public String getSoaCorrelationId() {
		return soaCorrelationId;
	}

	public void setSoaCorrelationId(String soaCorrelationId) {
		this.soaCorrelationId = soaCorrelationId;
	}

	public String getSoaAppId() {
		return soaAppId;
	}

	public void setSoaAppId(String soaAppId) {
		this.soaAppId = soaAppId;
	}
}
