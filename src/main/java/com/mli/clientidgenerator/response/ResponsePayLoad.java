package com.mli.clientidgenerator.response;

import java.io.Serializable;

import com.mli.common.response.StatusEntity;

/**
 * @author Amit
 *
 */
public class ResponsePayLoad implements Serializable {
	private static final long serialVersionUID = -236104041546894972L;
	private HeaderEntity header;
	private StatusEntity msgInfo;
	private ResponseDataEntity payload;

	public ResponsePayLoad() {
		super();
	}

	public HeaderEntity getHeader() {
		return header;
	}

	public void setHeader(HeaderEntity header) {
		this.header = header;
	}

	public StatusEntity getMsgInfo() {
		return msgInfo;
	}

	public void setMsgInfo(StatusEntity msgInfo) {
		this.msgInfo = msgInfo;
	}

	public ResponseDataEntity getPayload() {
		return payload;
	}

	public void setPayload(ResponseDataEntity payload) {
		this.payload = payload;
	}
}
