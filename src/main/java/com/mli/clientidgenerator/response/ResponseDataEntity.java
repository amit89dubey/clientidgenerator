package com.mli.clientidgenerator.response;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**
 * @author Amit
 *
 */
@Component
public class ResponseDataEntity implements Serializable {
	private static final long serialVersionUID = -7064939756453472651L;
	private String clientId;
	private String appTransactionId;
	public ResponseDataEntity() {
		super();
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getAppTransactionId() {
		return appTransactionId;
	}
	public void setAppTransactionId(String appTransactionId) {
		this.appTransactionId = appTransactionId;
	}
}
