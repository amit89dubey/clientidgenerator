package com.mli.clientidgenerator.response;

import java.io.Serializable;

/**
 * @author Amit
 *
 */
public class APIResponse implements Serializable {
	private static final long serialVersionUID = -3852138617026039838L;
	private ResponsePayLoad response;

	public ResponsePayLoad getResponse() {
		return response;
	}

	public void setResponse(ResponsePayLoad response) {
		this.response = response;
	}
}
