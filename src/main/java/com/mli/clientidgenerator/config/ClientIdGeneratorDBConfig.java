package com.mli.clientidgenerator.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
/**
 * @author Amit
 *
 */
@Configuration
public class ClientIdGeneratorDBConfig
{
	MongoDatabase db;
	public ClientIdGeneratorDBConfig(MongoClient mongoClient, Environment env) {
		this.db=mongoClient.getDatabase(env.getProperty("spring.data.mongodb.database")).withWriteConcern(WriteConcern.MAJORITY);
	}
	
	@Bean(name="mongoDBConfig")
	public MongoDatabase mongoDb()
	{
		return db;
	}
}