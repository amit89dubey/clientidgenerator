package com.mli.clientidgenerator.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class APIRequest implements Serializable {
	private static final long serialVersionUID = 8322600149057349059L;
	private RequestData request;

	public RequestData getRequest() {
		return request;
	}
	public void setRequest(RequestData request) {
		this.request = request;
	}
}
