package com.mli.clientidgenerator.request;

import java.io.Serializable;

import com.mli.clientidgenerator.request.RequestPayLoad;
import com.mli.clientidgenerator.response.HeaderEntity;

public class RequestData implements Serializable {
	private static final long serialVersionUID = -1097212633660180509L;
	private HeaderEntity header;
	private RequestPayLoad payload;

	public HeaderEntity getHeader() {
		return header;
	}

	public void setHeader(HeaderEntity header) {
		this.header = header;
	}

	public RequestPayLoad getPayload() {
		return payload;
	}

	public void setPayload(RequestPayLoad payload) {
		this.payload = payload;
	}
}
