package com.mli.clientidgenerator.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * RequestPayLoad is the main entity from where all the request parameters.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class RequestPayLoad implements Serializable {
	private static final long serialVersionUID = 4619976193445820307L;
	private String appTransactionId;
	public String getAppTransactionId() {
		return appTransactionId;
	}
	public void setAppTransactionId(String appTransactionId) {
		this.appTransactionId = appTransactionId;
	}
}
