/**
 * @author Amit
 *
 */
package com.mli.clientidgenerator.dao;

import com.mli.clientidgenerator.request.APIRequest;
import com.mli.clientidgenerator.response.APIResponse;
import com.mli.clientidgenerator.response.HeaderEntity;

public interface ClientIdGeneratorDao {
	public String generateClientID();
	public String saveClientIDDetails(HeaderEntity headerData , String clientId, String appTransactionId);
	public String saveClientIdGeneratorAuditLog(APIResponse apiResponse, APIRequest clientIdReq, String clientId); 
}
