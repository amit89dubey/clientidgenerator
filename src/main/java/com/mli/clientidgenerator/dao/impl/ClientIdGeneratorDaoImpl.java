/**
 * @author Amit
 *
 */
package com.mli.clientidgenerator.dao.impl;

import java.util.Map;

import org.bson.BsonDateTime;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mli.clientidgenerator.dao.ClientIdGeneratorDao;
import com.mli.clientidgenerator.request.APIRequest;
import com.mli.clientidgenerator.response.APIResponse;
import com.mli.clientidgenerator.response.HeaderEntity;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.result.InsertOneResult;

@Repository
public class ClientIdGeneratorDaoImpl implements ClientIdGeneratorDao  
{
	@Autowired Environment env;
	
	@Autowired
	@Qualifier("mongoDBConfig")
	MongoDatabase db;

	private static final String SEQUENCE = "sequence";
	private static final String ID = "_id";
	private static final String SOACORRELATIONID = "soaCorrelationId";
	private static final String SOAAPPID = "soaAppId";
	private static final String GENERATEDCLIENTID = "generatedClientId";
	private static final String APPTRANSACTIONID = "appTransactionId";
	private static final String MSGCODE = "msgCode";
	private static final String SERVICEREQUEST = "serviceRequest";
	private static final String SERVICERESPONSE = "ServiceResponse";
	private static final String CREATEDDT = "createdDt";
	private static final String CREATEDBY = "createdBy";
	private static final String UPDATEDDT = "updatedDt";
	private static final String UPDATEDBY = "updatedBy";
	
	private static final String APPLICATION = "clientidgenerator api";
	
	@Override
	public String generateClientID() 
	{
		Document queryFilter=new Document();
		queryFilter.append(ClientIdGeneratorDaoImpl.ID, env.getProperty("com.mli.mongodb.collection.counter.id"));
		Document update=new Document();
		update.append("$inc", new Document(ClientIdGeneratorDaoImpl.SEQUENCE, 1));
		
		FindOneAndUpdateOptions options=new FindOneAndUpdateOptions();
		Document projectionDocument=new Document();
		projectionDocument.append(ClientIdGeneratorDaoImpl.ID, 0);
		projectionDocument.append(ClientIdGeneratorDaoImpl.SEQUENCE, 1);
		options.projection(projectionDocument);
		
		MongoCollection coll=db.getCollection(env.getProperty("com.mli.mongodb.collection.counter"));
		Object resturnObj=coll.findOneAndUpdate( queryFilter, update,options);
		return ((Document)resturnObj).get(ClientIdGeneratorDaoImpl.SEQUENCE)+"";
	}

	@Override
	public String saveClientIDDetails(HeaderEntity headerData , String clientId, String appTransactionId) {
		Document insert=new Document();
		insert.append(ClientIdGeneratorDaoImpl.ID,clientId);
		insert.append(ClientIdGeneratorDaoImpl.SOACORRELATIONID,headerData.getSoaCorrelationId());
		insert.append(ClientIdGeneratorDaoImpl.SOAAPPID,headerData.getSoaAppId());
		insert.append(ClientIdGeneratorDaoImpl.APPTRANSACTIONID,appTransactionId);
		insert.append(ClientIdGeneratorDaoImpl.CREATEDDT,new BsonDateTime(System.currentTimeMillis()));
		insert.append(ClientIdGeneratorDaoImpl.CREATEDBY,ClientIdGeneratorDaoImpl.APPLICATION);
		insert.append(ClientIdGeneratorDaoImpl.UPDATEDDT,new BsonDateTime(System.currentTimeMillis()));
		insert.append(ClientIdGeneratorDaoImpl.UPDATEDBY,ClientIdGeneratorDaoImpl.APPLICATION);
		
		MongoCollection coll=db.getCollection(env.getProperty("com.mli.mongodb.collection.saving"));
		Object resturnObj=coll.insertOne(insert);
		return ((InsertOneResult)resturnObj).getInsertedId().asString().getValue();
	}
	
	public String saveClientIdGeneratorAuditLog(APIResponse apiResponse, APIRequest clientIdReq, String clientId) {
		ObjectMapper om = new ObjectMapper();
		Document insert=new Document();
		insert.append(ClientIdGeneratorDaoImpl.GENERATEDCLIENTID,clientId);
		insert.append(ClientIdGeneratorDaoImpl.SOACORRELATIONID,clientIdReq.getRequest().getHeader().getSoaCorrelationId());
		insert.append(ClientIdGeneratorDaoImpl.SOAAPPID,clientIdReq.getRequest().getHeader().getSoaAppId());
		insert.append(ClientIdGeneratorDaoImpl.APPTRANSACTIONID,apiResponse.getResponse().getPayload()!=null?apiResponse.getResponse().getPayload().getAppTransactionId():null);
		insert.append(ClientIdGeneratorDaoImpl.MSGCODE,apiResponse.getResponse().getMsgInfo().getMsgCode());
		insert.append(ClientIdGeneratorDaoImpl.SERVICEREQUEST,new Document(om.convertValue(clientIdReq, new TypeReference<Map<String, Object>>() {})));
		insert.append(ClientIdGeneratorDaoImpl.SERVICERESPONSE,new Document(om.convertValue(apiResponse, new TypeReference<Map<String, Object>>() {})));
		insert.append(ClientIdGeneratorDaoImpl.CREATEDDT,new BsonDateTime(System.currentTimeMillis()));
		insert.append(ClientIdGeneratorDaoImpl.CREATEDBY,ClientIdGeneratorDaoImpl.APPLICATION);
		insert.append(ClientIdGeneratorDaoImpl.UPDATEDDT,new BsonDateTime(System.currentTimeMillis()));
		insert.append(ClientIdGeneratorDaoImpl.UPDATEDBY,ClientIdGeneratorDaoImpl.APPLICATION);
		
		MongoCollection coll=db.getCollection(env.getProperty("com.mli.mongodb.collection.audit"));
		Object resturnObj=coll.insertOne(insert);
		return ((InsertOneResult)resturnObj).getInsertedId().asString().getValue();
	}
}
