package com.mli.clientidgenerator.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mli.clientidgenerator.request.APIRequest;
import com.mli.clientidgenerator.response.APIResponse;
import com.mli.clientidgenerator.service.ClientIdGeneratorService;
@RestController
@RequestMapping("/api/clientid")
public class ClientIdGeneratorRestController
{

	private static Logger logger = LogManager.getLogger(ClientIdGeneratorRestController.class);

	@Autowired ClientIdGeneratorService customerService;
	
	@PostMapping(value = "/generate/v1", consumes = {"application/JSON"}, produces = {"application/JSON"})
	public ResponseEntity<APIResponse> processClientIDGeneration(@RequestBody APIRequest customerIdReq,BindingResult bindingResult) 
	{
		logger.info("processCustomerIDGeneration : Started");
		return customerService.generateClientID(customerIdReq, bindingResult);
	}
}