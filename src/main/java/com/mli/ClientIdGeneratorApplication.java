package com.mli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientIdGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientIdGeneratorApplication.class, args);
	}
}
