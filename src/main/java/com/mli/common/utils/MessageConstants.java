package com.mli.common.utils;

/**
 * @author ad01084
 *
 */
public final class MessageConstants 
{
	private MessageConstants() {
	    throw new IllegalStateException("MessageConstants class");
	  }
	public static final String SUCCESS = "Success";
	public static final String FAILURE = "Failure";

	public static final String C200 = "200";
	public static final String C200DESC = "Response Generated Successfully";
	public static final String C201DESC = 	"Document Uploaded successfully";
	public static final String C203DESC = "Transaction Timeout";
	

	public static final String C500 = "500";
	public static final String C500DESC = "There seems to be something wrong. Please try after sometime.";
	public static final String C501DESC = "Web service not able to write the data";
	public static final String C502DESC = "There seems to be something wrong. As the response didn't contain token.";
	public static final String C503DESC = "There seems to be something wrong. As the response was empty from the server.";

	public static final String C600 = "600";
	public static final String C600DESC = "Validation check fail! Please verify your request json mandatory fields could not be empty and invalid key!!";
	public static final String C601DESC = "Validation check fail! Please verify your request json mendatory fields i.e. Invalid Number Of ClientId Requested!!";
	


	//////////////DB related Error code/////////////
	public static final String C700 = "700";
	public static final String C700DESC = "Data not found from backend";

	public static final String C701 = "701";
	public static final String C701DESC = "There seems to be something wrong at backend.";
	public static final String C702DESC = "Client ID does not exist";
	public static final String C703DESC="No client ID exist";

	/////Proposal generation Specific
	public static final String DUPLICATE_MSG = "Duplicate transaction id";
	public static final String PROPOSAL_MSG = "Policy has been delivered";
	public static final String PROPOSAL_EMPTY_MSG = "Policy Numbers in DB for this plan is '0',please fill the DB!";

	public static final String C_999 = "999";
	public static final String C_999_MSG = "Partial Success";

	
	public static final String APPID_COLLECTION="com.mli.auth.appid";
}


