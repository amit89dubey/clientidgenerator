package com.mli.common.response;

import java.io.Serializable;

import org.springframework.stereotype.Component;
/**
 * @author Amit
 *
 */
@Component
public class StatusEntity implements Serializable {
	private static final long serialVersionUID = -1073062729008244880L;
	private String msg;
	private String msgDescription;
	private String msgCode;
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getMsgDescription() {
		return msgDescription;
	}
	public void setMsgDescription(String msgDescription) {
		this.msgDescription = msgDescription;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
}
