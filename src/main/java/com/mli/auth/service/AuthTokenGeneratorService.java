package com.mli.auth.service;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.mli.auth.request.CognitoApiRequest;
import com.mli.auth.response.CognitoApiResponse;

public interface AuthTokenGeneratorService {
	public ResponseEntity<CognitoApiResponse> generateCognitoToken(CognitoApiRequest tokenReq,BindingResult bindingResult);
}
