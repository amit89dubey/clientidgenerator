package com.mli.auth.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mli.auth.request.CognitoApiRequest;
import com.mli.auth.request.CognitoRequestPayload;
import com.mli.auth.response.CognitoApiResponse;
import com.mli.auth.response.CognitoResponse;
import com.mli.auth.response.CognitoResponseFailure;
import com.mli.auth.response.CognitoResponsePayload;
import com.mli.auth.response.CognitoResponseSuccess;
import com.mli.auth.service.AuthTokenGeneratorService;
import com.mli.common.response.StatusEntity;
import com.mli.common.utils.MessageConstants;
@Service
public class AuthTokenGeneratorServiceImpl implements AuthTokenGeneratorService {

	private static Logger logger = LogManager.getLogger(AuthTokenGeneratorServiceImpl.class);
	@Autowired Environment env;

	private static final String ACCESS_TOKEN = "access_token";
	private static final String CONTENT_TYPE_URLENCODED = "application/x-www-form-urlencoded";
	private static final String HTTP_METHOD_POST = "POST";

	@Override
	public ResponseEntity<CognitoApiResponse> generateCognitoToken(CognitoApiRequest tokenReq, BindingResult bindingResult) {

		CognitoApiResponse apiResponse = new CognitoApiResponse();
		CognitoResponse response = new CognitoResponse();
		StatusEntity msgInfo = new StatusEntity();
		CognitoResponsePayload responsePayload=null;
		try
		{
			if (bindingResult.hasErrors() || authTokenRequestValidator(tokenReq))
			{
				msgInfo.setMsg(MessageConstants.FAILURE);
				msgInfo.setMsgCode(MessageConstants.C600);
				msgInfo.setMsgDescription(MessageConstants.C600DESC);
				response.setMsgInfo(msgInfo);
				apiResponse.setResponse(response);
				logger.debug(MessageConstants.C600DESC);
				return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
			}
			else
			{
				ThreadContext.push(tokenReq.getRequest().getHeader().getCorrelationId());
				logger.debug("Validation Success Processing further");
				response.setHeader(tokenReq.getRequest().getHeader());
				responsePayload = generateCognitoToken(tokenReq.getRequest().getPayload(),msgInfo);
			} 
		} 
		catch (Exception e)
		{
			msgInfo.setMsg(MessageConstants.FAILURE);
			msgInfo.setMsgCode(MessageConstants.C500);
			msgInfo.setMsgDescription(MessageConstants.C500DESC);
			responsePayload = new CognitoResponseFailure(e.getMessage());
			logger.error("We face Exception while generating unique client ID : "+e);
		}
		finally
		{
			response.setMsgInfo(msgInfo);
			response.setPayload(responsePayload);
			apiResponse.setResponse(response);
			logger.info("processAuthTokenGeneration : Ended");
			ThreadContext.pop();
		}
		return new ResponseEntity<>(apiResponse, HttpStatus.OK);
	}

	private boolean authTokenRequestValidator(CognitoApiRequest tokenReq)
	{
		List<String> appidList = Arrays.asList(env.getProperty(MessageConstants.APPID_COLLECTION, String[].class));
		return tokenReq==null 
				|| tokenReq.getRequest()==null 
				|| tokenReq.getRequest().getHeader()==null
				|| tokenReq.getRequest().getHeader().getAppId()==null
				|| tokenReq.getRequest().getHeader().getAppId().isEmpty()
				|| tokenReq.getRequest().getHeader().getCorrelationId()==null
				|| tokenReq.getRequest().getHeader().getCorrelationId().isEmpty()
				|| tokenReq.getRequest().getPayload()==null
				|| tokenReq.getRequest().getPayload().getClientId()==null
				|| tokenReq.getRequest().getPayload().getClientId().isEmpty()
				|| tokenReq.getRequest().getPayload().getClientSecret()==null
				|| tokenReq.getRequest().getPayload().getClientSecret().isEmpty()
				|| appidList==null
				|| !appidList.contains(tokenReq.getRequest().getHeader().getAppId());
	}


	public CognitoResponsePayload generateCognitoToken(CognitoRequestPayload cognitoRequest, StatusEntity msgInfo) throws IOException
	{
		logger.info("Inside generateCognitoToken method. request details: " + cognitoRequest.toString());
		CognitoResponsePayload responsePayload = null;
		String cognitoUrl = null;
		String clientId = cognitoRequest.getClientId();
		String clientSecret = cognitoRequest.getClientSecret();

		// Generate the cognito url and hit the rest api
		if (clientId != null && clientSecret != null) 
		{
			cognitoUrl = generateCognitoUrl(clientId, clientSecret);
			String outputJson = fetchCognitoToken(cognitoUrl);
			if (outputJson != null && outputJson.contains(ACCESS_TOKEN)) 
			{
				ObjectMapper om =  new ObjectMapper();
				Map<String, String> responseData = om.readValue(outputJson, Map.class);
				msgInfo.setMsg(MessageConstants.SUCCESS);
				msgInfo.setMsgCode(MessageConstants.C200);
				msgInfo.setMsgDescription(MessageConstants.C200DESC);
				responsePayload = new CognitoResponseSuccess(responseData.get(ACCESS_TOKEN),String.valueOf(responseData.get("expires_in")),responseData.get("token_type"));
			} 
			else if (outputJson != null)
			{
				msgInfo.setMsg(MessageConstants.FAILURE);
				msgInfo.setMsgCode(MessageConstants.C500);
				msgInfo.setMsgDescription(MessageConstants.C502DESC);
				responsePayload = new CognitoResponseFailure(MessageConstants.C502DESC);
			}
			else
			{
				msgInfo.setMsg(MessageConstants.FAILURE);
				msgInfo.setMsgCode(MessageConstants.C500);
				msgInfo.setMsgDescription(MessageConstants.C503DESC);
				responsePayload = new CognitoResponseFailure(MessageConstants.C503DESC);
			}
		}
		return responsePayload;
	}

	private String generateCognitoUrl(String clientId, String clientSecret) 
	{
		logger.info("Inside generateCognitoUrl method.");
		String initialUrlPart = env.getProperty("com.mli.auth.cognito.token.generation.url");
		StringBuilder cognitoUrlBuilder = new StringBuilder();
		cognitoUrlBuilder.append(initialUrlPart).append("client_id=").append(clientId)
		.append("&client_secret=").append(clientSecret)
		.append("&grant_type=").append(env.getProperty("com.mli.auth.cognito.token.grant.type"))
		.append("&Content-Type=").append(CONTENT_TYPE_URLENCODED);
		logger.debug("Final Cognito Url: " + cognitoUrlBuilder.toString());
		return cognitoUrlBuilder.toString().trim();
	}

	private String fetchCognitoToken(String cognitoUrl) throws IOException 
	{
		logger.info("Inside method fetchCognitoToken. Cognito url: " + cognitoUrl);
		String outputJson = null;
		URL url = new URL(cognitoUrl);
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setRequestMethod(HTTP_METHOD_POST);
		con.setRequestProperty("Content-Type", CONTENT_TYPE_URLENCODED);
		try 
		{
			int responseCode = con.getResponseCode();
			if (responseCode != HttpURLConnection.HTTP_OK) {
				throw new IOException("Failed to get token. HTTP error code : " + responseCode);
			}
			BufferedReader outputResponseReader = new BufferedReader(new InputStreamReader((con.getInputStream())));
			String output = null;
			while ((output = outputResponseReader.readLine()) != null) 
			{
				logger.debug("Output from Server : "+output);
				if (output.contains(ACCESS_TOKEN)) 
				{
					logger.debug("Found access_token : "+output);
					outputJson = output;
				}
			}
			logger.info("outputJson : " + outputJson);
			con.disconnect();
			outputResponseReader.close();
		} catch (UnknownHostException e) {
			con.disconnect();
		}
		return outputJson;
	}
}
