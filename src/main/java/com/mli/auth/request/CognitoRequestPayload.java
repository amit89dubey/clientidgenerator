package com.mli.auth.request;

import java.io.Serializable;

public class CognitoRequestPayload implements Serializable{

	private static final long serialVersionUID = -7380524846334076105L;
	private String clientId;
	private String clientSecret;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	@Override
	public String toString() {
		return "CognitoRequestPayload [clientId=" + clientId + ", clientSecret=" + clientSecret + "]";
	}
}
