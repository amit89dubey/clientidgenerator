package com.mli.auth.request;

import java.io.Serializable;

/**
 * @author Amit
 *
 */
public class CognitoApiRequest implements Serializable {
	private static final long serialVersionUID = 1764459008958311224L;
	private CognitoRequest request;

	public CognitoRequest getRequest() {
		return request;
	}

	public void setRequest(CognitoRequest request) {
		this.request = request;
	}
}
