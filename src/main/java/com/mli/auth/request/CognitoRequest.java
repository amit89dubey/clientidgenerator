package com.mli.auth.request;

import java.io.Serializable;

import com.mli.auth.response.Header;

/**
 * @author Amit
 *
 */
public class CognitoRequest implements Serializable{
	private static final long serialVersionUID = -6997392808897284764L;
	private Header header;
	private CognitoRequestPayload payload;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public CognitoRequestPayload getPayload() {
		return payload;
	}

	public void setPayload(CognitoRequestPayload payload) {
		this.payload = payload;
	}
}
