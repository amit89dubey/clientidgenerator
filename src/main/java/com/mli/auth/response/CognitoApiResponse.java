package com.mli.auth.response;

import java.io.Serializable;

/**
 * @author Amit
 *
 */
public class CognitoApiResponse implements Serializable{
	private static final long serialVersionUID = 589029513054232452L;
	private CognitoResponse response;

	public CognitoResponse getResponse() {
		return response;
	}

	public void setResponse(CognitoResponse response) {
		this.response = response;
	}
}
