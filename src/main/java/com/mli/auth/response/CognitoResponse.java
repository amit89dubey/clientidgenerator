package com.mli.auth.response;

import java.io.Serializable;

import com.mli.common.response.StatusEntity;


/**
 * @author Amit
 *
 */
public class CognitoResponse implements Serializable {
	private static final long serialVersionUID = 5506451777126059410L;
	private Header header;
	private StatusEntity msgInfo;
	private CognitoResponsePayload payload;
	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public StatusEntity getMsgInfo() {
		return msgInfo;
	}
	public void setMsgInfo(StatusEntity msgInfo) {
		this.msgInfo = msgInfo;
	}
	public CognitoResponsePayload getPayload() {
		return payload;
	}
	public void setPayload(CognitoResponsePayload payload) {
		this.payload = payload;
	}
}
