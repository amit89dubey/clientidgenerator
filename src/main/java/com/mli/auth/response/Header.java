package com.mli.auth.response;

import java.io.Serializable;

/**
 * @author Amit
 *
 */
public class Header implements Serializable{
	private static final long serialVersionUID = -363173094090336640L;
	private String appId;
	private String correlationId;
	
	public String getAppId() {
		return appId;
	}
	
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	public String getCorrelationId() {
		return correlationId;
	}
	
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
}
