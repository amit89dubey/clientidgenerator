package com.mli.auth.response;

/**
 * @author Amit
 *
 */
public class CognitoResponseFailure implements CognitoResponsePayload {
	private static final long serialVersionUID = -9057834247120778634L;
	private String errorMessage;
	
	public CognitoResponseFailure(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
