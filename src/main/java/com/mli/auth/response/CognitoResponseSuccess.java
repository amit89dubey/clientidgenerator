package com.mli.auth.response;

/**
 * @author Amit
 *
 */
public class CognitoResponseSuccess implements CognitoResponsePayload {
	private static final long serialVersionUID = -5419128104725274268L;
	private String accessToken;
	private String expiresIn;
	private String tokenType;
	
	public CognitoResponseSuccess(String accessToken, String expiresIn, String tokenType) {
		super();
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
		this.tokenType = tokenType;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
}
