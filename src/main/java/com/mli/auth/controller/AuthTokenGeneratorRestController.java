package com.mli.auth.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mli.auth.request.CognitoApiRequest;
import com.mli.auth.response.CognitoApiResponse;
import com.mli.auth.service.AuthTokenGeneratorService;
@RestController
@RequestMapping("/api/auth")
public class AuthTokenGeneratorRestController
{

	private static Logger logger = LogManager.getLogger(AuthTokenGeneratorRestController.class);
	@Autowired AuthTokenGeneratorService authTokenGeneratorService;

	@PostMapping(value = "/token/generate/v1", consumes = {"application/JSON"}, produces = {"application/JSON"})
	public ResponseEntity<CognitoApiResponse> processAuthTokenGeneration(@RequestBody CognitoApiRequest tokenReq,BindingResult bindingResult) 
	{
		logger.info("processAuthTokenGeneration : Started");
		return authTokenGeneratorService.generateCognitoToken(tokenReq, bindingResult);
	}
}